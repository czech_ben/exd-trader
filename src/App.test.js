import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

// basic test to make sure submit button renders on page load
test('renders submit react button', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/Submit Order/i);
  expect(linkElement).toBeInTheDocument();
});
