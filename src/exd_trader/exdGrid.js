import React, {useState, useEffect}  from 'react';
import './exdTrader.css';

import { connect } from "react-redux";

import { AgGridReact } from 'ag-grid-react';
import { PageHeader, Spin } from 'antd';

function EXDGrid(props) {

  // state represents whether the grid is loading and also the last update to the grid
  const [loading, setLoading] = useState(false);
  const [lastUpdated, setLastUpdated] = useState("");
  const [orders, setOrders] = useState([]);

  // if loading changes to true, set a 2 second timeout to mimic actual remote
  // loading functionality
  useEffect(() => {
    if (loading === true) {
      setTimeout(function () {
        setLoading(false);
      }, 2000);
    }
  }, [loading]);

  // when a new order is detected, set last updated to the timestamp of that order
  // and mimic a "loading" state. This would be handled in the redux action/reducer in
  // a real loading scenario but it was implemented this way for simplicity sake.
  useEffect(() => {
    setLoading(true);
    if (props.orders.length > 0) {
      setLastUpdated(props.orders.slice(-1)[0].timestamp.toLocaleString());
      setOrders(props.orders);
    }
  }, [props.orders]);

  // column definitions for the ag-grid
  const columns = [
    { headerName: "Action", field: "action", width:120,
      cellClassRules: {
            "green-cell": function(params) {
              return params.value === 'Buy';
            },
            "red-cell": function(params) {
              return params.value === 'Sell';
            }
          },
    },
    { headerName: "Symbol", field: "symbol", width:120, resizable: true, sortable: true },
    { headerName: "Quantity", field: "quantity", width:120, resizable: true, sortable: true },
    { headerName: "TIF", field: "timeInForce", width:120, resizable: true, sortable: true },
    { headerName: "Order Type", field: "orderType", width:120, resizable: true, sortable: true },
    { headerName: "Price", field: "price", width:120, valueFormatter: (params) => (params.value !== null ? "$" + params.value.toFixed(2) : ""), resizable: true, sortable: true},
    { headerName: "Stop Price", field: "stopPrice", width:120, valueFormatter: (params) => (params.value !== null ? "$" + params.value.toFixed(2) : ""), resizable: true, sortable: true },
    { headerName: "Comment", field: "comment", width:250, tooltip: (params) => params.value, resizable: true, sortable: true }
  ]

  // render a header with last updated state and the order grid
  return (
    <div>
      <div className='exd-header'>
        <PageHeader
          title="EXD Orders"
          subTitle={"Last Updated Time: " + lastUpdated}
        />
      </div>
      {loading ? <Spin className="exd-spinner" size="large" /> :
        <div className="ag-theme-balham" style={ {height: '250px', width: '100%'} }>
          <AgGridReact
              columnDefs={columns}
              rowData={orders}>
          </AgGridReact>
        </div>
      }
    </div>
  );
}

// orders are mapped from the redux store
function mapStateToProps(state) {
  const { orders } = state
  return { orders: orders.orders }
}

export default connect(mapStateToProps)(EXDGrid);
