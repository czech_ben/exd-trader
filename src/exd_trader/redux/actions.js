import { ADD_ORDER } from "./actionTypes";

export const addOrder = order => ({
  type: ADD_ORDER,
  payload: {
    order
  }
});
