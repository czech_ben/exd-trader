import { ADD_ORDER } from "../actionTypes";

const initialState = {
  orders: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case ADD_ORDER: {
      const { order } = action.payload;
      console.log("ADDING ORDER TO STORE");
      return {
        ...state,
        orders: [...state.orders, order]
      };
    }
    default:
      return state;
  }
}
