import React from 'react';
import EXDTicket from './exdTicket.js'
import EXDGrid from './exdGrid.js'
import SplitterLayout from 'react-splitter-layout';

function EXDTrader() {
  return (
    // SplitterLayout component adds horizontal splitter on page
    <SplitterLayout vertical="false" percentage={true} secondaryInitialSize={45}>
      <EXDTicket/>
      <EXDGrid/>
    </SplitterLayout>
  );
}

export default EXDTrader;
