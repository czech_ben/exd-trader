import React, {useState} from 'react';
import './exdTrader.css';

import { connect } from "react-redux";
import { addOrder } from "./redux/actions";

import { PageHeader, Row, Col, Select, Typography, Input, InputNumber, Button, Tooltip, Modal} from 'antd';

const { Option } = Select;
const { Text } = Typography;
const {TextArea } = Input

function EXDTicket(props) {

  // state represents all the available fields of an order
  const [action, setAction] = useState('Buy');
  const [symbol, setSymbol] = useState(null);
  const [quantity, setQuantity] = useState(1);
  const [timeInForce, setTimeInForce] = useState('DAY');
  const [orderType, setOrderType] = useState('Market');
  const [price, setPrice] = useState(null);
  const [stopPrice, setStopPrice] = useState(null);
  const [comment, setComment] = useState(null);

  // all availble options for dropdowns in ticket
  const symbols = ["AAPL", "MSFT", "GOOGL", "VZ", "MMM", "NFLX", "FB", "TWTR", "AMZN", "EBAY"]
  const tifs = ["DAY", "GTC", "FOK", "IOC"]
  const actions = ["Buy", "Sell"]
  const orderTypes = ["Market", "Limit"]

  const options = {
    symbols: symbols,
    tifs: tifs,
    actions: actions,
    orderTypes: orderTypes
  }

  // order counter for 10th order failure functionality
  let orderCounter = 0;

  // when order is submitted, increment order counter and display error for every 10th order
  // also, trigger the addOrder redux action to update redux store
  const onSubmitOrder = () => {
      orderCounter += 1;
      if (orderCounter % 10 === 0) {
        Modal.error({
          title: 'Order time has elapsed',
        });
      }
      else {
        const timestamp = new Date();
        props.addOrder({action, symbol, quantity, timeInForce, orderType, price, stopPrice, comment, timestamp})
      }
  };

  // validation logic for order form
  const validateOrder = () => {
      return (orderType === 'Limit' && ((price === null) || (stopPrice === null))) || (!symbol || (quantity === null));
  };

  // helper function to render options for dropdown
  const renderOptions = (type) => {
      return options[type].map((value) => <Option value={value}>{value}</Option> )
  };

  // render the form fields and submit button
  return (
    <div>
      <div className='exd-header'>
        <PageHeader
          title="EXD Trader"
          subTitle="Order Entry"
        />
      </div>
      <div className='exd-ticket'>
        <Row gutter={[{ xs: 16, sm: 32, md: 48, lg: 64 }]}>
          <Col xs={12} lg={6}>
            <Text className="label-text" strong>Action:</Text><br/>
            <Select className={action === 'Buy' ? "green-select" : "red-select"} defaultValue="Buy" onChange={setAction}>
              {renderOptions("actions")}
            </Select>
          </Col>
          <Col xs={12} lg={6}>
            <Text className="label-text" strong>Symbol:</Text><br/>
            <Select showSearch placeholder="<Enter Symbol>" style={{ width: 150 }} onChange={setSymbol}>
              {renderOptions("symbols")}
            </Select>
          </Col>
          <Col xs={12} lg={6}>
            <Text className="label-text" strong>Quantity:</Text><br/>
            <InputNumber precision={0} min={1} max={999} defaultValue={1} onChange={setQuantity} />
          </Col>
          <Col xs={12} lg={6}>
            <Text className="label-text" strong>Price:</Text><br/>
            <InputNumber precision={2} min={0.00} step={0.01} onChange={setPrice} />
          </Col>
        </Row>
        <Row gutter={[{ xs: 16, sm: 32, md: 48, lg: 64 }]}>
          <Col xs={12} lg={6}>
            <Text className="label-text" strong>Order Type:</Text><br/>
            <Select defaultValue="Market" style={{ width: 150 }} onChange={setOrderType}>
              {renderOptions("orderTypes")}
            </Select>
          </Col>
          <Col xs={12} lg={6}>
            <Text className="label-text" strong>TIF:</Text><br/>
            <Select defaultValue="DAY" style={{ width: 150 }} onChange={setTimeInForce}>
              {renderOptions("tifs")}
            </Select>
          </Col>
          <Col xs={{ span: 12, offset: 12 }} lg={{ span: 6, offset: 6 }}>
            <Text className="label-text" strong>Stop Price:</Text><br/>
            <InputNumber precision={2} min={0.00} step={0.01} onChange={setStopPrice} />
          </Col>
        </Row>
        <Row gutter={[{ xs: 16, sm: 32, md: 48, lg: 64 }]}>
          <Col xs={12} lg={6}>
            <Text className="label-text" strong>Comment:</Text><br/>
            <TextArea rows={3} onChange={(event) => setComment(event.target.value)}/>
          </Col>
          <Col xs={{ span: 6, offset: 0 }} lg={{ span: 6, offset: 6 }}>
            <Tooltip title={validateOrder() ? "Symbol and quantity fields required. Price and stop price required for limit orders." : ""}>
              <Button className='submit-order-button' disabled={validateOrder()} type="primary" onClick={onSubmitOrder}>Submit Order</Button>
            </Tooltip>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default connect(
  null,
  { addOrder }
)(EXDTicket);
