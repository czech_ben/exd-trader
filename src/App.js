import React from 'react';
import './App.css';
import EXDTrader from './exd_trader/exdTrader.js';

function App() {
  return (
    <div className="App">
        <EXDTrader/>
    </div>
  );
}

export default App;
